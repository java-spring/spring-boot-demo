package redises;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import redises.utils.RedisesUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisesApplicationTests {

	@Autowired
	private RedisesUtils redisesUtils;

	@Test
	public void test() {
        for (int i = 0; i < 10; i++) {
            redisesUtils.stringSet(""+i,""+i);
        }
	}

    @Test
    public void testDel() {
        for (int i = 0; i < 10; i++) {
            redisesUtils.redisDel(""+i);
        }
    }

}
