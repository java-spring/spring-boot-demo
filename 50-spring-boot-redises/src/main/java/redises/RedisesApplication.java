package redises;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisesApplication.class, args);
	}
}
