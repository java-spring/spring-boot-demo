package redises.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisesUtils {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void stringSet(String key, String value){
        stringRedisTemplate.opsForValue().set(key,value);
    }

    public void redisDel(String key){
        stringRedisTemplate.delete(key);
    }
}


