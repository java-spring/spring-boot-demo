package spring.boot.mybatis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootMybatisApplicationTests {

	@Autowired
	private SysUserDao sysUserDao;

	@Test
	public void contextLoads() {
	    List<SysUser> sysUserList = sysUserDao.findList("blues");
        for (int i = 0; i < sysUserList.size(); i++) {
            System.out.println("id="+sysUserList.get(i).getId()+"   and   name="+sysUserList.get(i).getName());
        }
	}

}
