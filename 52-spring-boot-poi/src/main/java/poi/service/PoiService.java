package poi.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import poi.entity.User;
import poi.entity.Word;
import poi.utils.PoiUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PoiService {


    public void user(HttpServletResponse response) {
        User user1 = new User("001", "张三",new Date());
        User user2 = new User("002", "李四",new Date());
        User user3 = new User("003", "王五",new Date());
        User user4 = new User("004", "赵六",new Date());

        List<User> data = new ArrayList<>();
        data.add(user1);
        data.add(user2);
        data.add(user3);
        data.add(user4);

        try {
            PoiUtils.exportExcel("用户日志记录", "用户表", true, User.class, data, response, "用户日志记录");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Word> word(MultipartFile file) throws Exception {
        return PoiUtils.importStreamExcel(file.getInputStream(), Word.class);
    }


    public List<Word> word(File file) {
        return PoiUtils.importFileExcel(file, Word.class);
    }
}
