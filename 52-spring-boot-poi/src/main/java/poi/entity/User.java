package poi.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Builder
public class User implements Serializable {


    @Excel(name = "id",orderNum = "1",width = 20)
    private String id;
    @Excel(name = "name",orderNum = "2",width = 20)
    private String name;
    @Excel(name = "time",orderNum = "3",width = 20,exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date time;

}
