package cache.controller;

import cache.entity.User;
import cache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/insert/{id}/{name}")
    public User insert(@PathVariable String id, @PathVariable String name){
        return userService.insert(new User(id,name));
    }

    @DeleteMapping("/deleteById/{id}")
    public Integer deleteById(@PathVariable String id){
        return userService.deleteById(id);
    }

    @GetMapping("/selectById/{id}")
    public User selectById(@PathVariable String id){
        return userService.selectById(id);
    }
}
