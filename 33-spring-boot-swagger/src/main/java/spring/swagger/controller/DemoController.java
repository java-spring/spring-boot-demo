package spring.swagger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guo
 */
@Api(description = "05-demo apis")
@RestController
public class DemoController {


    @ApiOperation(value="06-find接口", notes="07-根据id获取find")
    @ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "String", paramType = "path")
    @GetMapping("/find/{id}")
    public String findById(@PathVariable("id") String id){
        return "hello swagger:"+id;
    }
}
