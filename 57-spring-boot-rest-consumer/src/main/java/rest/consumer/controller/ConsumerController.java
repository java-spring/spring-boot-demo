package rest.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author guo
 */
@RestController
public class ConsumerController {


    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("rest/hello")
    public String restHello(){
        String hello = restTemplate.getForObject("http://localhost:8057/hello",String.class);
        return hello;
    }

    @GetMapping("rest/hello/param")
    public String restHelloParam(){
        Map map = new HashMap(4);
        map.put("name","name");
        map.put("value","value");
        String helloParam = restTemplate.getForObject("http://localhost:8057/hello/param?name={name}&value={value}",String.class,map);
        return helloParam;
    }


    @GetMapping("rest/hello/post")
    public String restHelloPost(){
        String hello = restTemplate.postForObject("http://localhost:8057/hello/post",null,String.class);
        return hello;
    }


    @GetMapping("rest/hello/post/param")
    public String restHelloPostParam(){
        Map map = new HashMap(4);
        map.put("name","name");
        map.put("value","value");
        String hello = restTemplate.postForObject("http://localhost:8057/hello/post/param",map,String.class);
        return hello;
    }

}
