package jta.atomikos.two.mapper;

import jta.atomikos.model.two.Two;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author guo
 */
public interface TwoMapper {

    @Select("select id,two from two where id=#{id}")
    Two selectById(@Param("id") String id);


    @Insert("insert into two(id,two) values(#{id},#{two})")
    Integer insert(Two two);
}
