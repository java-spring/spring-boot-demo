package h2.controller;

import h2.entity.User;
import h2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/insert/{id}/{name}")
    public Integer insert(@PathVariable String id, @PathVariable String name){
        return userService.insert(id,name);
    }

    @GetMapping("/selectById/{id}")
    public User selectById(@PathVariable String id){
        return userService.selectById(id);
    }
}
