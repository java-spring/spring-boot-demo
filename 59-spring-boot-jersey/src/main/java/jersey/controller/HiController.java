package jersey.controller;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * @author guo
 */
@Component
@Path("hi")
public class HiController {

    @GET
    @Path("user")
    public String hi(){
        return "hi jersey";
    }

}
