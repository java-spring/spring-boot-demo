package many.datasource.service;

import many.datasource.entity.User;
import many.datasource.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired(required = false)
    private UserMapper userMapper;



    public int insertUser(User user) {
        return userMapper.insertUser(user.getId(), user.getName());
    }

    public List<User> selectUser() {
        return userMapper.selectUserList();
    }

}
