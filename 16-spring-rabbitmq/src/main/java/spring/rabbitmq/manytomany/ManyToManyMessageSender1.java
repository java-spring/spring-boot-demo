package spring.rabbitmq.manytomany;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManyToManyMessageSender1 {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send() {
        for (int i = 1; i <= 10; i++) {
            String context = "111--message-" + i + " : "+ System.currentTimeMillis();
            System.out.println(context);
            rabbitTemplate.convertAndSend("manytomany", context);
        }
    }

}
